package com.finnos.awave.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by doogoon on 2016. 7. 18..
 */
@Data
@Entity
@Table(name = "DEV_M")
public class DEVM implements Serializable {
    private static final long serialVersionUID = 2348146646775420782L;

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Integer DEV_M_IDX;
    private String DEV_M_POSNUM;
    private String DEV_M_NM;
    private String DEV_M_TYPE;
    private String DEV_M_PTOK;
    private String DEV_M_DTOK;


    // common
    private Date SYS_REG_DATE;
    private Integer SYS_REG_OBJ_IDX;
    private Integer SYS_REG_PRO_IDX;
    private Date SYS_UPDATE_DATE;
    private Integer SYS_UPDATE_OBJ_IDX;
    private Integer SYS_UPDATE_PRO_IDX;
    private String SYS_IS_DELETE;

    // relation
    private Integer BU_M_IDX;
}

package com.finnos.awave.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by doogoon on 2016. 7. 18..
 */
@Data
@Entity
@Table(name = "BU_M")
public class BUM implements Serializable {
    private static final long serialVersionUID = 2348146646775420782L;

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Integer BU_M_IDX;
    private String BU_M_NM;
    private String BU_M_CNUM;
    private String BU_M_PNUM;
    private String BU_M_ADDR1;
    private String BU_M_ADDR2;
    private String BU_M_ZIP;
    private String SYS_IS_DELETE;
    private String BU_M_TYPE;
    private Date SYS_REG_DATE;
    private Date SYS_UPDATE_DATE;

    private Integer SYS_REG_OBJ_IDX;
    private Integer SYS_REG_PRO_IDX;
    private Integer SYS_UPDATE_OBJ_IDX;
    private Integer SYS_UPDATE_PRO_IDX;
}

package com.finnos.awave.repository;

import com.finnos.awave.model.BUM;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by doogoon on 2016. 7. 18..
 */
public interface BUMRepository extends JpaRepository<BUM, Integer> {

    int countBy();

}

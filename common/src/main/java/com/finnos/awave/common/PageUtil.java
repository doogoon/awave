package com.finnos.awave.common;

import java.util.Map;

/**
 * Created by doogoon on 2016. 7. 18..
 */
public class PageUtil {

    private int blockSize 	= 10;

    public String getPagingLink(int totalPageCount, int totalItemCount, int currentPage, String requestUri, Map params ){


        if( totalPageCount == 0 ) totalPageCount = 1;
//		StringBuffer paramsBf = new StringBuffer();
//
//		Set<Object> keySet = params.keySet();
//
//		for (Object key : keySet) {
//
//			Object obj = params.get(key.toString());
//
//			if( obj == null ) continue;
//
//			String value = obj.toString();
//
//			paramsBf.append(key + "=" + value + "&");
//		}

        String request = requestUri + "?"; // + paramsBf.toString();

//		if( totalItemCount % pageSize > 0) totalPageCount++;

        int currentBlock = ( currentPage / blockSize ) + 1;

        if( currentPage % blockSize == 0 ) currentBlock--;

        int prevPage = ((currentPage-1)==0)?1:(currentPage);
        int nextPage = ((currentPage+1)>totalPageCount)?totalPageCount:(currentPage);


        int endBlock = currentBlock * blockSize;
        if (endBlock > totalPageCount) endBlock = totalPageCount;

        StringBuffer buf = new StringBuffer();

        buf.append("<div class=\"pagination\"><ul>");

        if( currentPage > 1 ){

            buf.append("<li class='arrow_left'><a href='" + request + "page=" + (prevPage-2) + "'><i class=\"fa fa-angle-left\"></i></a></li>");

        }else{

            buf.append("<li class='arrow_left'><a class='disable'><i class=\"fa fa-angle-left\"></a></i></li>");
        }



        int startBlock = ((currentBlock * blockSize) - blockSize) + 1;

        for(int i = startBlock; i <= endBlock; i++){

            if( i == startBlock ){

                if ( i == currentPage){

                    buf.append("<li><a href=\"#\" class=\"active\">" +  i  + "</a></li>");

                } else {

                    buf.append("<li><a href='" + request + "page=" + (i-1) + "'>" +  i  + "</a></li>");
                }

            }else{

                if ( i == currentPage){

                    buf.append("<li><a href=\"#\" class=\"active\">" +  i  + "</a></li>");

                } else {

                    buf.append("<li><a href='" + request + "page=" + (i-1) + "'>" +  i  + "</a></li>");
                }

            }


        }


        if(currentPage < totalPageCount){

            buf.append("<li class='arrow_right'><a href='" + request + "page=" + nextPage + "'><i class=\"fa fa-angle-right\"></i></a></li>");


        }else{

            buf.append( "<li class='arrow_right'><a class='disable'><i class=\"fa fa-angle-right\"></a></i></li>");
        }

        buf.append("</ul></div>");

        return  buf.toString();

    }


    public int getBlockSize() {
        return blockSize;
    }

    public void setBlockSize(int blockSize) {
        this.blockSize = blockSize;
    }

}

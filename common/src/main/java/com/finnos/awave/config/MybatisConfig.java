package com.finnos.awave.config;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;

@Configuration
@EnableTransactionManagement
@MapperScan(value = { "com.finnos.awave.mapper" })
public class MybatisConfig
{
	private String mybatisConfigLocation = "classpath:mapper/mybatis-config.xml";
	//private String mybatisMapperLocations = "classpath:net/xenix/kiwoom/crowd/mappers/*.xml";

	@Autowired
	private ApplicationContext applicationContext;

	@Bean
	public SqlSessionFactory sqlSessionFactory(DataSource dataSource) throws Exception {
		SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
		sqlSessionFactoryBean.setDataSource(dataSource);
		//sqlSessionFactoryBean.setTypeAliasesPackage("net.xenix.kiwoom.model");
		sqlSessionFactoryBean.setConfigLocation(applicationContext.getResource(mybatisConfigLocation));
		//sqlSessionFactoryBean.setMapperLocations(applicationContext.getResources(mybatisMapperLocations));

		Resource bumMapper                          = new ClassPathResource("mapper/bu-m-mapper.xml");

		sqlSessionFactoryBean.setMapperLocations(new Resource[]{bumMapper});

		return sqlSessionFactoryBean.getObject();
	}
}

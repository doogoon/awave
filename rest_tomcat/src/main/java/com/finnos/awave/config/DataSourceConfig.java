package com.finnos.awave.config;

import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.lookup.JndiDataSourceLookup;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.Properties;

/**
 * Created by doogoon on 2016. 7. 19..
 */
@Configuration
@EnableJpaRepositories(basePackages = {"com.finnos.awave"})
@EnableTransactionManagement
public class DataSourceConfig {

    @Value("${spring.datasource.dataSourceClassName}") private String driverClassName;
    @Value("${spring.datasource.url}") private String url;
    @Value("${spring.datasource.username}") private String username;
    @Value("${spring.datasource.password}") private String password;
    @Value("${spring.datasource.validationQuery}") private String validationQuery;
    @Value("${spring.jpa.properties.hibernate.dialect}") private String dialect;

    @Bean
    public DataSource dataSource()
    {
        BasicDataSource bds = new BasicDataSource();
        bds.setDriverClassName(driverClassName);
        bds.setUrl(url);
        bds.setUsername(username);
        bds.setPassword(password);
        bds.setValidationQuery(validationQuery);
        return bds;
    }

    @Bean
    public DataSourceTransactionManager transactionManager() {
        return new DataSourceTransactionManager(dataSource());
    }


//    @Bean
//    public PlatformTransactionManager transactionManager(EntityManagerFactory emf) {
//        JpaTransactionManager transactionManager = new JpaTransactionManager();
//        transactionManager.setEntityManagerFactory(emf);
//
//
//        return transactionManager;
//    }

    @Bean
    LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
        entityManagerFactoryBean.setDataSource(dataSource());
        entityManagerFactoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        entityManagerFactoryBean.setPackagesToScan("com.finnos.awave");

        Properties jpaProperties = new Properties();
        jpaProperties.put("hibernate.dialect", dialect);

//		//Configures the used database dialect. This allows Hibernate to create SQL
//		//that is optimized for the used database.
//
//
//		//Specifies the action that is invoked to the database when the Hibernate
//		//SessionFactory is created or closed.
//		jpaProperties.put("hibernate.hbm2ddl.auto",
//				env.getRequiredProperty("hibernate.hbm2ddl.auto")
//		);
//
//		//Configures the naming strategy that is used when Hibernate creates
//		//new database objects and schema elements
        jpaProperties.put("hibernate.ejb.naming_strategy",
                "org.hibernate.cfg.ImprovedNamingStrategy"
        );
//
//		//If the value of this property is true, Hibernate writes all SQL
//		//statements to the console.
        jpaProperties.put("hibernate.show_sql",
                "true"
        );
//
        //If the value of this property is true, Hibernate will format the SQL
        //that is written to the console.
        jpaProperties.put("hibernate.format_sql",
                "true"
        );


        jpaProperties.put("hibernate.enable_lazy_load_no_trans", "true");

        entityManagerFactoryBean.setJpaProperties(jpaProperties);

        return entityManagerFactoryBean;
    }

}

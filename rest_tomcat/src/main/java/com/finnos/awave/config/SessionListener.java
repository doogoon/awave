package com.finnos.awave.config;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 * Created by doogoon on 2016. 7. 19..
 */
public class SessionListener implements HttpSessionListener {

    @Override
    public void sessionCreated(HttpSessionEvent event) {

        event.getSession().setMaxInactiveInterval(0);
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent event) {

    }

}

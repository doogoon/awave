package com.finnos.awave.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;

/**
 * Created by doogoon on 2016. 7. 19..
 */
abstract public class RootController {

    private final Logger log = LoggerFactory.getLogger(RootController.class);

    public static void main(String[] args) throws ParseException {

    }

}

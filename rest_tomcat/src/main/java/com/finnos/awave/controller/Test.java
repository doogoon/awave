package com.finnos.awave.controller;

import com.finnos.awave.mapper.BuMMapper;
import com.finnos.awave.repository.BUMRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by doogoon on 2016. 7. 19..
 */
@RestController
public class Test extends RootController {

    private final Logger log = LoggerFactory.getLogger(Test.class);

    @Autowired
    private BuMMapper buMMapper;

    @Autowired
    private BUMRepository bumRepository;

    @RequestMapping(value = "/test", method = RequestMethod.GET)
    public Object testMethod(

    ) {
        Map<String, Object> result = new HashMap<String, Object>();
        Map<String, Object> params = new HashMap<String, Object>();

        log.info("haha");

        int bumCount = bumRepository.countBy();
        long bumCount2 = bumRepository.count();

        int bumCount3 = buMMapper.selectBUMCount(params);


        return result;
    }

}

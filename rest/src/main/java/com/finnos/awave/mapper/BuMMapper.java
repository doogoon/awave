package com.finnos.awave.mapper;

import java.util.Map;

/**
 * Created by doogoon on 2016. 7. 18..
 */
public interface BuMMapper {

    int selectBUMCount(Map<String, Object> params);

}

package com.finnos.awave.mapper;

import java.util.Map;

/**
 * Created by doogoon on 2016. 7. 18..
 */
public interface UserMapper {

    int selectUsersCount(Map<String, Object> params);

}

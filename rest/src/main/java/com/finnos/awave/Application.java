package com.finnos.awave;

import com.google.gson.Gson;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Created by doogoon on 2016. 7. 13..
 */
@ComponentScan(basePackages = {"com.finnos.awave", "kr.pe.okjsp"})
@SpringBootApplication(
        exclude = {
//                RedisHttpSessionConfiguration.class,
                DataSourceTransactionManagerAutoConfiguration.class,
                DataSourceAutoConfiguration.class
        })
//@MapperScan(basePackages="com.finnos.awave.mybatis.mapper")
//@EnableScheduling
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    public Gson gson(){

        return new Gson();
    }

}

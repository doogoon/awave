package com.finnos.awave.database;

import com.finnos.awave.config.JdbcConnectionSettings;
import oracle.jdbc.pool.OracleDataSource;
import org.apache.commons.dbcp.BasicDataSource;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.sql.SQLException;

/**
 * Created by doogoon on 2016. 7. 15..
 */
@Configuration
@Lazy
@EnableTransactionManagement
//@MapperScan(basePackages = {"com.finnos.awave.repository"})
public class DatabaseConfig {


    @Autowired
    private JdbcConnectionSettings jdbcConnectionSettings;

    @Bean
    public DataSource dataSource() throws SQLException {

//        OracleDataSource dataSource = new OracleDataSource();
//        dataSource.setUser(jdbcConnectionSettings.getUsername());
//        dataSource.setPassword(jdbcConnectionSettings.getPassword());
//        dataSource.setURL(jdbcConnectionSettings.getUrl());
//        dataSource.setImplicitCachingEnabled(true);
//        dataSource.setFastConnectionFailoverEnabled(true);
//        return dataSource;

        BasicDataSource ds = new BasicDataSource();

        ds.setDriverClassName("oracle.jdbc.driver.OracleDriver");
        ds.setUsername(jdbcConnectionSettings.getUsername());
        ds.setPassword(jdbcConnectionSettings.getPassword());
        ds.setUrl(jdbcConnectionSettings.getUrl());
        ds.setValidationQuery("/* ping */ select 1 from dual");

        return ds;
    }

    @Bean
    public SqlSessionFactory sqlSessionFatory() throws Exception{
        SqlSessionFactoryBean sqlSessionFactory = new SqlSessionFactoryBean();
        sqlSessionFactory.setDataSource(dataSource());
        sqlSessionFactory.setConfigLocation(new ClassPathResource("mapper/mybatis-config.xml"));
        //sqlSessionFactory.setMapperLocations(new PathMatchingResourcePatternResolver().getResources("classpath:/mappers/*.xml"));
        sqlSessionFactory.setFailFast(true);



        Resource usersMapper                          = new ClassPathResource("mapper/users-mapper.xml");
        Resource bumMapper                          = new ClassPathResource("mapper/bu-m-mapper.xml");

        sqlSessionFactory.setMapperLocations(new Resource[]{usersMapper, bumMapper});


        return sqlSessionFactory.getObject();
    }

    @Bean
    public SqlSessionTemplate sqlSession(SqlSessionFactory sqlSessionFactory) {
        return new SqlSessionTemplate(sqlSessionFactory);
    }


}
